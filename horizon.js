class Horizon {

  constructor() {

    this.version = "1.0.0";
    this.releaseName = "Stormborn";

    console.log("Horizon " + this.releaseName + " " + this.version + " starting...");
    this.changeHostBrowserTitle("Loading Horizon...");

    this.assetesArray = [
      "styles/styles.css",
      "styles/colors.css",
      "styles/fonts.css",
      "styles/spinner.css",
      "styles/materialdesignicons.min.css",
      "translations.json",
      "icons/login-45-fff@2x.png",
      "icons/right-arrow-filled-45-fff@2x.png",
      "icons/add-45-000@2x.png",
      "icons/play-45-000@2x.png",
      "icons/console-45-000@2x.png",
      "icons/translation-45-fff@2x.png",
      "icons/grid-45-000@2x.png",
      "icons/browse-page-45-000@2x.png",
      "gifs/spinner-0.7s-32px-fff-0f0f0f@2x.gif",
      "gifs/spinner-4.8s-100px-fff-000@2x.gif"
    ];

    this.loadedAssets = {};
    this.LUID = 0; // Local Unic ID
    this.windows = [];
    this.shortcuts = []
    this.appsLayout = "custom";
    this.activeApp = null;
    this.api = new API();

    this.waitForDomLoad().then(() => {

      this.uiCreateHorizonLoader();

      this.loadAssets(this.assetesArray).then(() => {

        this.uiUpdateHorizonLoader("Initializing translations", .76, 1);
        this.initTranslations();
        this.uiUpdateHorizonLoader("Subscribing to window errors", .77, 1);
        this.subscribeToHostBrowserErrors();

        this.changeHostBrowserTitle("LeanGate");

        this.uiUpdateHorizonLoader("Searching for user session", .8, 1);
        this.api.auth.status().then(authed => {

          this.uiUpdateHorizonLoader("Horizon is ready", 1, 1);
          this.uiDestroyHorizonLoader();

          if (authed) {
            console.log("User already authed");
            this.uiInitSystem();
          } else {
            console.log("User is not authed yet");
            this.uiCreateLoginWindow();
          }

        }).catch(err => {

          console.error("Failed to get auth status", err);

          this.uiUpdateHorizonLoader("Horizon is ready", 1, 1);
          this.uiDestroyHorizonLoader();

          this.uiCreateLoginWindow();

        });

      });

    });

  }

  async waitForDomLoad() {
    console.log("Waiting for DOM to become ready...");
    return new Promise((resolve, reject) => {
      let clock = setInterval(function() {
        if (document.readyState === "complete") {
          clearInterval(clock);
          console.log("DOM is ready");
          resolve();
        } else {
          console.log("DOM is still loading");
        }
      }, 15);
    });
  }

  async loadAssets(assetsArray) {
    console.log("Loading Horizon assets...");
    let proceededIterator = 0;
    return Promise.all(assetsArray.map(async uri => {
      let assetName = uri.split("/").pop();
      return new Promise((resolve, reject) => {
        this.loadAsset(uri)
          .then(() => {
            this.uiUpdateHorizonLoader(
              assetName,
              ++proceededIterator,
              assetsArray.length / 0.75 // 100% of loaded assets is a 75% of global load
            );
            console.log("Loaded asset " + assetName);
            resolve();
          }, () => {
            console.error("Asset load failed " + assetName);
            // TODO: show error or smth like that
            reject();
          });
      });
    }));
  }

  async loadAsset(uri) {

    const originURI = uri;
    uri = `/horizon/${uri}`;

    // TODO: handle resolve and reject more carefully

    return new Promise((resolve, reject) => {
      let type = uri.split(".").pop();
      let asset = null;
      switch (type) {
        case "js":
          asset = document.createElement('script');
          document.getElementsByTagName("head")[0].appendChild(asset);
          asset.setAttribute("type", "text/javascript");
          asset.addEventListener('load', function() {
            resolve();
          });
          asset.addEventListener('error', function(e) {
            reject(e);
          });
          asset.setAttribute("src", uri);
          this.loadedAssets[originURI] = asset;
          break;
        case "css":
          asset = document.createElement('link');
          document.getElementsByTagName("head")[0].appendChild(asset);
          asset.setAttribute("rel", "stylesheet");
          asset.setAttribute("type", "text/css");
          asset.addEventListener('load', function() {
            resolve();
          });
          asset.addEventListener('error', function(e) {
            reject(e);
          });
          asset.setAttribute("href", uri);
          this.loadedAssets[originURI] = asset;
          break;
        case "png":
          asset = document.createElement('img');
          asset.addEventListener('load', function() {
            resolve();
          });
          asset.addEventListener('error', function(e) {
            reject(e);
          });
          asset.setAttribute("src", uri);
          this.loadedAssets[originURI] = asset;
          break;
        case "svg":
          asset = document.createElement('img');
          asset.addEventListener('load', function() {
            resolve();
          });
          asset.addEventListener('error', function(e) {
            reject(e);
          });
          asset.setAttribute("src", uri);
          this.loadedAssets[originURI] = asset;
          break;
        case "gif":
          asset = document.createElement('img');
          asset.addEventListener('load', function() {
            resolve();
          });
          asset.addEventListener('error', function(e) {
            reject(e);
          });
          asset.setAttribute("src", uri);
          this.loadedAssets[originURI] = asset;
          break;
        case "json":
          Horizon.httpGET(uri).then((xhr) => {
            try {
              this.loadedAssets[originURI] = JSON.parse(xhr.responseText);
              console.log(`JSON ${originURI} parsed`, this.loadedAssets[originURI]);
              resolve();
            } catch (e) {
              console.error("Can't parse JSON structure in JSON asset " + originURI);
              reject(new Error("Can't parse JSON structure in JSON asset"));
            }
          }).catch((xhr) => {
            console.error("Failed to download asset " + originURI);
            reject(new Error(xhr.status + " " + xhr.statusText));
          });
          break;
        default:
          console.error("Unknown asset extension " + type + " of " + originURI);
          reject(new Error("Unknown asset extension"));
      }
    });
  }

  uiCreateSystemLoader() {

    let spinner = document.createElement("div");
    spinner.classList.add("spinner");

    let title = document.createElement("div");
    title.classList.add("title");
    this.translate("System is starting...", "LOADING_LOGON_SYSTEM_STARTING", title);

    let loader = document.createElement("div");
    loader.id = "systemLoader";
    loader.appendChild(spinner);
    loader.appendChild(title);

    document.body.appendChild(loader);

    this.uiDestroySystemLoader = () => {
      document.removeChild(loader);
    }

  }

  uiCreateHorizonLoader() {

    let fill = document.createElement("div");
    fill.classList.add("fill");

    let bar = document.createElement("div");
    bar.classList.add("bar");
    bar.appendChild(fill);

    let title = document.createElement("div");
    title.classList.add("title");
    title.innerText = "Horizon starting...";

    let loader = document.createElement("div");
    loader.id = "loader";
    loader.appendChild(bar);
    loader.appendChild(title);

    document.body.appendChild(loader);

    this.uiUpdateHorizonLoader = (name, proceeded, total) => {

      if (typeof proceeded !== "number") proceeded = 1;
      if (typeof total !== "number") total = 1;

      let percentage = proceeded / total * 100;
      if (isNaN(percentage)) percentage = 0;

      let percentageBeauty = (Math.floor(percentage * 100) / 100) + "%";
      console.log("Updating Horizon loader fill: " + percentageBeauty + " for " + name);
      fill.style.width = percentageBeauty;
      title.innerText = name;

    };

    let horizonBootVersion = document.createElement("div");
    horizonBootVersion.classList.add("bootVersion");
    horizonBootVersion.innerText = this.version;
    document.body.appendChild(horizonBootVersion);

    this.uiDestroyHorizonLoader = () => {
      this.uiUpdateHorizonLoader = () => {};
      document.body.removeChild(loader);
      document.body.removeChild(horizonBootVersion);
    }

  }

  uiCreateLoginWindow() {

    let title = document.createElement("h1");
    this.translate("Login", "AUTH_LOGIN_TITLE", title);

    let description = document.createElement("p");
    this.translate("Sign in to your account to continue.", "AUTH_LOGIN_DESCRIPTION", description);

    let usernameTitle = document.createElement("h6");
    usernameTitle.classList.add("username");
    this.translate("Username", "AUTH_LOGIN_USERNAME", usernameTitle);

    let usernameField = document.createElement("input");
    usernameField.id = "login";
    usernameField.type = "text";

    let passwordTitle = document.createElement("h6");
    passwordTitle.classList.add("password");
    this.translate("Password", "AUTH_LOGIN_PASSWORD", passwordTitle);

    let passwordField = document.createElement("input");
    passwordField.id = "password";
    passwordField.type = "password";

    let spinner = document.createElement("div");
    spinner.classList.add("spinner");
    spinner.classList.add("hidden");

    let errorMessage = document.createElement("div");
    errorMessage.id = "errorMessage";
    errorMessage.classList.add("hidden");

    let submitButton = document.createElement("div");
    submitButton.classList.add("disabled");
    submitButton.id = "signinSubmit";

    let submitButtonImage = document.createElement("div");
    submitButtonImage.classList.add("image");

    let submitButtonText = document.createElement("div");
    this.translate("Sign In", "AUTH_LOGIN_SIGNIN_BUTTON", submitButtonText);
    submitButtonText.classList.add("text");

    submitButton.appendChild(submitButtonText);
    submitButton.appendChild(submitButtonImage);

    let signinBox = document.createElement("div");
    signinBox.id = "horizonSignIn";
    signinBox.appendChild(title);
    signinBox.appendChild(description);
    signinBox.appendChild(usernameTitle);
    signinBox.appendChild(usernameField);
    signinBox.appendChild(passwordTitle);
    signinBox.appendChild(passwordField);
    signinBox.appendChild(spinner);
    signinBox.appendChild(errorMessage);
    signinBox.appendChild(submitButton);

    document.body.appendChild(signinBox);

    usernameField.focus();

    this.uiDestroyHorizonLoginWindow = () => {
      signinBox.remove();
    };

    this.uiAuthLoginWindow = () => {

      setTimeout(() => {

        errorMessage.classList.add("hidden");
        submitButton.classList.add("hidden");
        spinner.classList.remove("hidden");
        usernameField.disabled = true;
        passwordField.disabled = true;

        const login = usernameField.value;
        const password = passwordField.value;

        this.api.auth.auth(login, password).then(user => {

          if (user) {

            signinBox.classList.add("hidden");

            this.uiInitSystem();

            setTimeout(() => {
              this.uiDestroyHorizonLoginWindow();
            }, 500);

          } else {

            this.translate("Invalid login or password", "AUTH_LOGIN_ERROR_INVALID_CREDENTIALS", errorMessage);
            errorMessage.classList.remove("hidden");

            submitButton.classList.remove("hidden");
            spinner.classList.add("hidden");
            usernameField.disabled = false;
            passwordField.disabled = false;

          }

        }).catch(err => {

          console.log(err);

          this.translate("Failed to connect to server", "AUTH_LOGIN_ERROR_SERVER_FAILURE", errorMessage);
          errorMessage.classList.remove("hidden");

          submitButton.classList.remove("hidden");
          spinner.classList.add("hidden");
          usernameField.disabled = false;
          passwordField.disabled = false;

        });

      }, 20);
    };

    usernameField.addEventListener("keyup", (event) => {

      if (usernameField.value == "" || passwordField.value == "") {
        submitButton.classList.add("disabled");
      } else {
        submitButton.classList.remove("disabled");
        errorMessage.classList.add("hidden");
        if (event.key === "Enter") {
          this.uiAuthLoginWindow();
        }
      }

    });

    passwordField.addEventListener("keyup", (event) => {
      if (usernameField.value == "" || passwordField.value == "") {
        submitButton.classList.add("disabled");
      } else {
        submitButton.classList.remove("disabled");
        errorMessage.classList.add("hidden");
        if (event.key === "Enter") {
          this.uiAuthLoginWindow();
        }
      }
    });

    submitButton.addEventListener("mousedown", (event) => {
      if (usernameField.value !== "" && passwordField.value !== "")
        this.uiAuthLoginWindow();
    });

  }

  uiInitSystem() {

    let workspace = document.createElement("div");
    workspace.id = "workspace";

    let workspaceContainer = document.createElement("div");
    workspaceContainer.id = "workspaceContainer";
    workspaceContainer.classList.add("hidden");
    workspaceContainer.appendChild(workspace);

    document.body.appendChild(workspaceContainer);

    this.uiInitWorkspace(workspace);
    this.uiInitDock(workspace);


    workspaceContainer.classList.add("horizonBackground");


    setTimeout(() => {
      workspaceContainer.classList.remove("hidden");


    }, 200);

    /*setTimeout(() => {
        this.uiCreateHorizonDeveloperPanel();
    }, 400);*/

    document.body.addEventListener("contextmenu", (event) => {
      event.preventDefault();
      return false;
    });

  }

  uiInitDock(workspace) {

    let dock = document.createElement("div");
    dock.id = "dock";




    workspace.appendChild(dock);

  }

  uiInitWorkspace(workspace) {

    console.log("Init workspace...");


    let apps = [{
        "name": "Files",
        "icon": "/horizon/icons/demo/File-Explorer.png"
      },
      {
        "name": "Trash",
        "icon": "/horizon/icons/demo/Recycle-Full.png"
      },
      {
        "name": "Store",
        "icon": "/horizon/icons/demo/Microsoft-Store.png",
        "url": "https://www.microsoft.com/ru-ru/store/apps/windows"
      },
      {
        "name": "Calculator",
        "icon": "/horizon/icons/demo/Calculator.png"
      },
      {
        "name": "Excel",
        "icon": "/horizon/icons/demo/Excel.png"
      },
      {
        "name": "PowerPoint",
        "icon": "/horizon/icons/demo/PowerPoint.ico"
      },
      {
        "name": "OneNote",
        "icon": "/horizon/icons/demo/OneNote.png"
      },
      {
        "name": "Word",
        "icon": "/horizon/icons/demo/Word.png"
      },
      {
        "name": "Photos",
        "icon": "/horizon/icons/demo/Photos.png"
      },
      {
        "name": "Calendar",
        "icon": "/horizon/icons/demo/Calendar.png"
      },
      {
        "name": "Skype",
        "icon": "/horizon/icons/demo/Skype.png",
        "url": "https://web.skype.com/"
      },
      {
        "name": "Mail",
        "icon": "/horizon/icons/demo/Mail.png"
      },
      {
        "name": "Teams",
        "icon": "/horizon/icons/demo/Teams.png"
      },
      {
        "name": "OneDrive",
        "icon": "/horizon/icons/demo/OneDrive.png"
      },
      {
        "name": "SharePoint",
        "icon": "/horizon/icons/demo/SharePoint.png",
        "url": "http://demo.themekita.com/ready-pro/livepreview/examples/"
      },
    ];


    let visibleHeight = document.body.offsetHeight - 50 - 80; // 25+25 - margin from top and bottom, 80 - dock size
    let visibleWidth = document.body.offsetWidth;

    let elements = 8;

    let blockHeightPercentage = 100 / elements;

    let blockHeight = (visibleHeight / 100 * blockHeightPercentage) - 10;
    let blockWidth = blockHeight * 0.9;

    let columnElementsSize = Math.floor(100 / blockHeightPercentage);

    let columns = Math.ceil(apps.length / columnElementsSize);

    for (let columnI = 0; columnI < columns; columnI++) {

      console.log("Making column " + columnI);

      for (let elementI = 0; elementI < columnElementsSize; elementI++) {

        console.log("Making element " + elementI + " in column " + columnI);

        let appI = (columnI * columnElementsSize) + elementI;

        if (appI === apps.length) break;

        let appIcon = apps[appI];

        let elementTop = (elementI * (blockHeight + 10) + 25);
        let elementLeft = (25 + (columnI * (blockWidth + 10)));

        let element = document.createElement("div");
        element.classList.add("element");
        element.classList.add("hidden");
        element.style.width = (blockWidth - 2) + "px";
        element.style.height = (blockHeight - 2) + "px";
        element.style.top = elementTop + "px";
        element.style.left = elementLeft + "px";

        let icon = document.createElement("div");
        icon.classList.add("icon");
        icon.style.height = (blockHeight - 30) + "px";
        icon.style.backgroundImage = "url('" + appIcon.icon + "')";

        let title = document.createElement("div");
        title.classList.add("title");
        title.innerText = appIcon.name;

        element.appendChild(icon);
        element.appendChild(title);

        workspace.appendChild(element);









        let elementMoving = false;
        let initialClickX = 0;
        let initialClickY = 0;
        let initialTop = 0;
        let initialLeft = 0;
        let initialWidth = 0;
        let initialHeight = 0;

        element.addEventListener("mousedown", (event) => {

          if (!event.ctrlKey) {
            let elements = workspace.getElementsByClassName("element");
            for (let i = 0; i < elements.length; i++) {
              elements[i].classList.remove("active");
            }
          }


          if (event.ctrlKey && element.classList.contains("active")) {
            element.classList.remove("active");
          } else {
            element.classList.add("active");
          }


          initialClickX = event.pageX;
          initialClickY = event.pageY;
          initialTop = element.getBoundingClientRect().top;
          initialLeft = element.getBoundingClientRect().left;
          initialWidth = element.getBoundingClientRect().width;
          initialHeight = element.getBoundingClientRect().height;

          elementMoving = true;

          this.shortcuts.push(element);

          let maxZIndex = 0;
          for (let someWindow of this.windows) {
            let someZIndex = parseInt(window.getComputedStyle(someWindow.dom).zIndex);
            if (someZIndex > maxZIndex) {
              maxZIndex = someZIndex;
            }
          }
          for (let shortcut of this.shortcuts) {
            let someZIndex = parseInt(window.getComputedStyle(shortcut).zIndex);
            if (someZIndex > maxZIndex) {
              maxZIndex = someZIndex;
            }
          }
          element.style.zIndex = maxZIndex + 1;

        });

        document.body.addEventListener("mouseup", (event) => {
          if (elementMoving) {

            element.classList.remove("noAnimation");
            element.classList.add("longAnimation");
            setTimeout(() => {
              element.classList.remove("longAnimation");
            }, 500);

            // todo: move to calculated new position instead of initial
            element.style.left = initialLeft + "px";
            element.style.top = initialTop + "px";

            elementMoving = false;
            initialClickX = 0;
            initialClickY = 0;
            initialTop = 0;
            initialLeft = 0;
            initialWidth = 0;
            initialHeight = 0;

          }
        });

        element.addEventListener("dragstart", (event) => {
          event.preventDefault();
          return false;
        });

        let animatingTransition = false;

        let mouseMoveListener = (event) => {

          event.preventDefault();

          if (elementMoving) {

            let calculatedX = event.pageX - (initialClickX - initialLeft);
            let calculatedY = event.pageY - (initialClickY - initialTop);

            let diffX = calculatedX - initialLeft;
            let diffY = calculatedY - initialTop;

            if (Math.abs(diffX) < initialWidth && Math.abs(diffY) < initialHeight) {

              if (element.classList.contains("noAnimation")) {
                element.classList.remove("noAnimation");
              }

              element.style.left = initialLeft + (diffX / 10) + "px";
              element.style.top = initialTop + (diffY / 10) + "px";

            } else {

              if (!element.classList.contains("noAnimation") && !animatingTransition) {
                animatingTransition = true;
                setTimeout(() => {
                  element.classList.add("noAnimation");
                  animatingTransition = false;
                }, 500);
              }

              element.style.top = calculatedY + "px";
              element.style.left = calculatedX + "px";

            }

            /*console.log(event);*/

          }

        };

        document.body.addEventListener("mousemove", mouseMoveListener);









        element.addEventListener("dblclick", (event) => {

          console.log("Opening window for " + appIcon.name, event);

          let windowObject = this.uiCreateWindow({
            icon: appIcon.icon,
            title: appIcon.name,
            callObject: element
          });

          if (typeof appIcon["url"] !== "undefined") {
            windowObject.loadURL(appIcon["url"]);
          }

        });

        if (title.clientHeight > 22) {
          element.classList.add("doubleTitle");
        }

        element.classList.remove("hidden");

      }

    }

    workspace.addEventListener("mousedown", (event) => {

      if (event.target === workspace) {

        let elements = workspace.getElementsByClassName("element");
        for (let i = 0; i < elements.length; i++) {
          elements[i].classList.remove("active");
        }

        if (this.activeApp !== null) {
          this.activeApp.unfocus();
          this.activeApp = null;
        }

      }

    });

  }

  uiCreateHorizonDeveloperPanel() {

    let panel = document.createElement("div");
    panel.id = "uiDeveloperPanel";
    panel.classList.add("hidden");

    let buttonsToDo = [
      ["add", "DEVTOOLS_HORIZON_CREATE", (event) => {
        let windowObject = this.uiCreateWindow();

      }, "mdi-application"],
      ["closeAll", "DEVTOOLS_HORIZON_CLOSEALL", (event) => {
        for (let i = 0; i < this.windows.length; i++) {
          setTimeout(() => {
            this.windows[0].close();
          }, 30 * i)
        }
      }, "mdi-close-box-multiple"],
      ["play", "DEVTOOLS_HORIZON_RUN", (event) => {

      }, "mdi-play"],
      ["terminal", "DEVTOOLS_HORIZON_TERMINAL", (event) => {

      }, "mdi-console"],
      ["language", "DEVTOOLS_HORIZON_LANGUAGE", (event) => {
        let translations = Object.keys(this.getTranslations());
        let currentTranslationI = translations.indexOf(this.language) + 1;
        if (currentTranslationI > translations.length - 1) {
          currentTranslationI = 0;
        }
        this.changeTranslation(translations[currentTranslationI]);
      }]
    ];

    for (let i = 0; i < buttonsToDo.length; i++) {

      let iconClass = buttonsToDo[i][0];
      let iconTextID = buttonsToDo[i][1];
      let iconEventListener = buttonsToDo[i][2];
      let iconImageClass = buttonsToDo[i][3];

      let image = document.createElement("div");
      image.classList.add("icon");
      if (typeof iconImageClass !== "undefined") {
        image.classList.add("mdi");
        image.classList.add(iconImageClass);
      }

      let text = document.createElement("div");
      text.classList.add("name");
      this.translate(iconTextID, iconTextID, text);

      let icon = document.createElement("div");
      icon.classList.add(iconClass);

      icon.appendChild(image);
      icon.appendChild(text);

      icon.classList.add("item");

      icon.addEventListener("mousedown", iconEventListener);

      panel.appendChild(icon);

    }

    document.body.appendChild(panel);
    setTimeout(() => {
      panel.classList.remove("hidden");
    }, 100);

  }

  uiCreateWindow(parameters) {

    if (typeof parameters !== "object") parameters = {};
    if (typeof parameters["icon"] !== "string") parameters["icon"] = "/horizon/icons/browse-page-45-000@2x.png";
    if (typeof parameters["size"] !== "object") parameters["size"] = {};
    if (typeof parameters["size"]["width"] !== "number") parameters["size"]["width"] = 600;
    if (typeof parameters["size"]["height"] !== "number") parameters["size"]["height"] = 400;
    if (typeof parameters["size"]["minWidth"] !== "number") parameters["size"]["minWidth"] = 170;
    if (typeof parameters["size"]["minHeight"] !== "number") parameters["size"]["minHeight"] = 130;
    if (typeof parameters["size"]["maxWidth"] !== "number") parameters["size"]["maxWidth"] = "";
    if (typeof parameters["size"]["maxHeight"] !== "number") parameters["size"]["maxHeight"] = "";

    let id = ++this.LUID;
    if (typeof parameters["title"] !== "string") parameters["title"] = "Window " + id;

    let horizonWindow = document.createElement("div");
    horizonWindow.classList.add("window");
    horizonWindow.classList.add("hidden");
    horizonWindow.id = "window" + id;
    document.getElementById("workspace").appendChild(horizonWindow);


    // App window control bar

    let windowControlBar = document.createElement("div");
    windowControlBar.classList.add("controlBar");
    horizonWindow.appendChild(windowControlBar);

    let windowIcon = document.createElement("div");
    windowIcon.classList.add("windowIcon");
    windowControlBar.appendChild(windowIcon);

    let windowTitle = document.createElement("div");
    windowTitle.classList.add("windowTitle");
    windowControlBar.appendChild(windowTitle);

    let windowAppControls = document.createElement("div");
    windowAppControls.classList.add("appControls");
    windowControlBar.appendChild(windowAppControls);


    // App window content

    let windowContent = document.createElement("div");
    windowContent.classList.add("content");
    horizonWindow.appendChild(windowContent);

    let windowDevControls = document.createElement("div");
    windowDevControls.classList.add("devControls");
    windowDevControls.classList.add("hidden");
    horizonWindow.appendChild(windowDevControls);


    // App window borders

    let topBorder = document.createElement("div");
    topBorder.classList.add("topBorder");
    horizonWindow.appendChild(topBorder);

    let rightBorder = document.createElement("div");
    rightBorder.classList.add("rightBorder");
    horizonWindow.appendChild(rightBorder);

    let bottomBorder = document.createElement("div");
    bottomBorder.classList.add("bottomBorder");
    horizonWindow.appendChild(bottomBorder);

    let leftBorder = document.createElement("div");
    leftBorder.classList.add("leftBorder");
    horizonWindow.appendChild(leftBorder);


    // App window corners

    let topRightCorner = document.createElement("div");
    topRightCorner.classList.add("topRightCorner");
    horizonWindow.appendChild(topRightCorner);

    let bottomRightCorner = document.createElement("div");
    bottomRightCorner.classList.add("bottomRightCorner");
    horizonWindow.appendChild(bottomRightCorner);

    let bottomLeftCorner = document.createElement("div");
    bottomLeftCorner.classList.add("bottomLeftCorner");
    horizonWindow.appendChild(bottomLeftCorner);

    let topLeftCorner = document.createElement("div");
    topLeftCorner.classList.add("topLeftCorner");
    horizonWindow.appendChild(topLeftCorner);

    let horizonWindowControls = {
      dom: horizonWindow,
      id: id,
      showDevControls: () => {
        windowDevControls.classList.remove("hidden");
        return horizonWindowControls;
      },
      hideDevContols: () => {
        windowDevControls.classList.add("hidden");
        return horizonWindowControls;
      },
      focus: () => {

        if (this.activeApp !== null && this.activeApp.id !== id) {
          this.activeApp.unfocus();
        }

        if (this.activeApp == null || this.activeApp.id !== id) {
          let maxZIndex = 0;
          for (let someWindow of this.windows) {
            let someZIndex = parseInt(window.getComputedStyle(someWindow.dom).zIndex);
            if (someZIndex > maxZIndex) {
              maxZIndex = someZIndex;
            }
          }
          for (let shortcut of this.shortcuts) {
            let someZIndex = parseInt(window.getComputedStyle(shortcut).zIndex);
            if (someZIndex > maxZIndex) {
              maxZIndex = someZIndex;
            }
          }
          horizonWindow.style.zIndex = maxZIndex + 1;
        }

        this.activeApp = horizonWindowControls;
        horizonWindow.classList.add("focused");

        console.log("Focused window " + id + " as layer " + horizonWindow.style.zIndex);
        return horizonWindowControls;

      },
      unfocus: () => {

        horizonWindow.classList.remove("focused");

      },
      close: () => {
        console.info("Window " + id + " is closing...");
        horizonWindow.classList.add("hidden");
        setTimeout(() => {

          horizonWindow.remove();

          let maxZIndexWindow = null;
          let maxZIndex = 0;
          for (let someWindow of this.windows) {
            let someZIndex = parseInt(window.getComputedStyle(someWindow.dom).zIndex);
            if (someZIndex > maxZIndex) {
              maxZIndex = someZIndex;
              maxZIndexWindow = someWindow;
            }
          }
          if (maxZIndexWindow !== null) maxZIndexWindow.focus();

        }, 120);
        document.body.removeEventListener("mousemove", mouseMoveListener);
        for (let i = 0; i < this.windows.length; i++) {
          if (this.windows[i].id === id) {
            this.windows.splice(i, 1);
            break;
          }
        }

        return null;
      },
      move: (x, y) => {
        horizonWindow.style.top = y + "px";
        horizonWindow.style.left = x + "px";
        return horizonWindowControls;
      },
      resizeWidth: (width) => {
        horizonWindow.style.width = width + "px";
        return horizonWindowControls;
      },
      resizeHeight: (height) => {
        horizonWindow.style.height = height + "px";
        return horizonWindowControls;
      },
      resize: (width, height) => {
        return horizonWindowControls
          .resizeWidth(width)
          .resizeHeight(height);
      },
      setMaxWidth: (width) => {
        horizonWindow.style.maxWidth = width + "px";
        return horizonWindowControls;
      },
      setMaxHeight: (height) => {
        horizonWindow.style.maxHeight = height + "px";
        return horizonWindowControls;
      },
      setMaxSize: (width, height) => {
        return horizonWindowControls
          .setMaxWidth(width)
          .setMaxHeight(height);
      },
      resetMaxWidth: () => {
        horizonWindow.style.maxWidth = "";
        return horizonWindowControls;
      },
      resetMaxHeight: () => {
        horizonWindow.style.maxHeight = "";
        return horizonWindowControls;
      },
      resetMaxSize: () => {
        return horizonWindowControls
          .resetMaxWidth()
          .resetMaxHeight();
      },
      setMinWidth: (width) => {
        horizonWindow.style.minWidth = width + "px";
        return horizonWindowControls;
      },
      setMinHeight: (height) => {
        horizonWindow.style.minHeight = height + "px";
        return horizonWindowControls;
      },
      setMinSize: (width, height) => {
        return horizonWindowControls
          .setMinWidth(width)
          .setMinHeight(height);
      },
      resetMinWidth: () => {
        horizonWindow.style.minWidth = "";
        return horizonWindowControls;
      },
      resetMinHeight: () => {
        horizonWindow.style.minHeight = "";
        return horizonWindowControls;
      },
      resetMinSize: () => {
        return horizonWindowControls
          .resetMinWidth()
          .resetMinHeight();
      },
      /*moveToInitialPosition: (x, y) => {


          let documentWidth = document.body.clientWidth;
          let documentHeight = document.body.clientHeight;

          let windowWidth = horizonWindow.clientWidth;
          let windowHeight = horizonWindow.clientHeight;

          let topLeftCornerX = (documentWidth / 2) - (windowWidth / 2);
          let topLeftCornerY = (documentHeight / 2) - (windowHeight / 2);

          console.log("Moving window " + id + " to initial position X:" + topLeftCornerX + ", Y:" + topLeftCornerX, documentWidth, documentHeight, windowWidth, windowHeight);

          if (typeof x === "number" && typeof y === "number") {

              let xDiff = x - topLeftCornerX;
              let yDiff = y - topLeftCornerY;

              console.log("Diff ", xDiff, yDiff);

              let easedXDiff = Math.floor(xDiff / 4);
              let easedYDiff = Math.floor(yDiff / 4);

              topLeftCornerX += easedXDiff;
              topLeftCornerY += easedYDiff;

          }

          horizonWindowControls.move(topLeftCornerX, topLeftCornerY);

          return horizonWindowControls;

      },*/
      moveToInitialPosition: (callObject, moveToPrePosition) => {

        let documentWidth = document.body.clientWidth;
        let documentHeight = document.body.clientHeight;

        let windowWidth = horizonWindow.clientWidth;
        let windowHeight = horizonWindow.clientHeight;

        let topLeftCornerX = 0;
        let topLeftCornerY = 0;

        if (typeof callObject == "object" && callObject) {

          let initialTop = callObject.getBoundingClientRect().top;
          let initialLeft = callObject.getBoundingClientRect().left;

          let callObjectHeight = callObject.getBoundingClientRect().height;
          let callObjectWidth = callObject.getBoundingClientRect().width;

          let windowMarginSideHeight = (callObjectHeight * 1.5);
          let windowMarginSideWidth = (callObjectWidth * 1.5);

          let windowMarginHeight = windowMarginSideHeight * 2;
          let windowMarginWidth = windowMarginSideWidth * 2;

          let callObjectTopLocationPercentage = initialTop / documentHeight * 100;
          let callObjectLeftLocationPercentage = initialLeft / documentWidth * 100;

          let windowScaledHeight = windowHeight + windowMarginHeight;
          let windowScaledWidth = windowWidth + windowMarginWidth;

          if (moveToPrePosition) {
            topLeftCornerX = initialLeft - (windowWidth / 100 * callObjectLeftLocationPercentage);
            topLeftCornerY = initialTop - (windowHeight / 100 * callObjectTopLocationPercentage);
          } else {
            topLeftCornerX = initialLeft + windowMarginSideWidth - (windowScaledWidth / 100 * callObjectLeftLocationPercentage);
            topLeftCornerY = initialTop + windowMarginSideHeight - (windowScaledHeight / 100 * callObjectTopLocationPercentage);
          }

        } else {
          topLeftCornerX = (documentWidth / 2) - (windowWidth / 2);
          topLeftCornerY = (documentHeight / 2) - (windowHeight / 2);
        }

        horizonWindowControls.move(topLeftCornerX, topLeftCornerY);
        return horizonWindowControls;

      },
      changeTitle: (titleText) => {
        windowTitle.innerText = titleText;
        return horizonWindowControls;
      },
      changeIcon: (iconURL) => {
        windowIcon.style.backgroundImage = "url('" + iconURL + "')";
        return horizonWindowControls;
      },
      loadURL: (url) => {

        let iframe = document.createElement("iframe");
        iframe.allowtransparency = true;
        iframe.classList.add("hidden");
        iframe.addEventListener("load", () => {
          iframe.classList.remove("hidden");
        });
        windowContent.appendChild(iframe);
        iframe.src = url;

        return horizonWindowControls;

      }
    };
    this.windows.push(horizonWindowControls);

    horizonWindowControls
      .changeTitle(parameters["title"])
      .changeIcon(parameters["icon"])
      .resize(parameters["size"]["width"], parameters["size"]["height"])
      .setMinSize(parameters["size"]["minWidth"], parameters["size"]["minHeight"])
      .setMaxSize(parameters["size"]["maxWidth"], parameters["size"]["maxHeight"])
      .focus();

    horizonWindowControls.moveToInitialPosition(typeof parameters["callObject"] === "object" ? parameters["callObject"] : null, true);

    setTimeout(() => {
      horizonWindow.classList.remove("hidden");
      horizonWindowControls
        .moveToInitialPosition(typeof parameters["callObject"] === "object" ? parameters["callObject"] : null, false);
    }, 50);


    let appControlButtons = [
      ["close", (event) => {
        event.preventDefault();
        horizonWindowControls.close();
      }],
      ["hide"],
      ["expand"]
    ];

    for (let i = 0; i < appControlButtons.length; i++) {

      let appControlButtonData = appControlButtons[i];
      let buttonAction = appControlButtonData[0];

      let appControlButton = document.createElement("div");
      appControlButton.classList.add(buttonAction);
      appControlButton.classList.add("circle");
      appControlButton.addEventListener("mousedown", appControlButtonData[1]);
      windowAppControls.appendChild(appControlButton);


    }


    let windowMoving = false;
    let windowResizing = false;

    let resizeTop = false;
    let resizeRight = false;
    let resizeBottom = false;
    let resizeLeft = false;

    let initialClickX = 0;
    let initialClickY = 0;
    let initialTop = 0;
    let initialLeft = 0;
    let initialWidth = 0;
    let initialHeight = 0;

    horizonWindow.addEventListener("mousedown", (event) => {
      horizonWindowControls.focus();
      initialClickX = event.pageX;
      initialClickY = event.pageY;
      initialTop = horizonWindow.getBoundingClientRect().top;
      initialLeft = horizonWindow.getBoundingClientRect().left;
      initialWidth = horizonWindow.getBoundingClientRect().width;
      initialHeight = horizonWindow.getBoundingClientRect().height;
    });

    horizonWindow.addEventListener("mouseup", (event) => {
      horizonWindow.classList.remove("noAnimation");
      windowMoving = false;
      windowResizing = false;
      resizeTop = false;
      resizeRight = false;
      resizeBottom = false;
      resizeLeft = false;
      initialClickX = 0;
      initialClickY = 0;
      initialTop = 0;
      initialLeft = 0;
      initialWidth = 0;
      initialHeight = 0;
    });

    windowControlBar.addEventListener("dragstart", (event) => {
      event.preventDefault();
      return false;
    });

    windowControlBar.addEventListener("mousedown", (event) => {
      event.preventDefault();
      if (event.target === windowControlBar || event.target === windowTitle) {
        windowMoving = true;
        horizonWindow.classList.add("noAnimation");
      }
    });

    let mouseMoveListener = (event) => {
      event.preventDefault();
      if (windowMoving) {

        let calculatedX = event.pageX - (initialClickX - initialLeft);
        let calculatedY = event.pageY - (initialClickY - initialTop);

        console.log("Window " + id + " move ", event.pageX, event.pageY, calculatedX, calculatedY);

        horizonWindowControls.move(calculatedX, calculatedY);

      }
      if (windowResizing) {

        let calculatedTop = initialTop;
        let calculatedLeft = initialLeft;
        let calculatedWidth = initialWidth;
        let calculatedHeight = initialHeight;

        // simple resize
        if (resizeRight) {
          calculatedWidth += event.pageX - initialClickX;
        }
        if (resizeBottom) {
          calculatedHeight += event.pageY - initialClickY;
        }

        // harder resize
        if (resizeTop) {
          calculatedTop += event.pageY - initialClickY;
          calculatedHeight -= event.pageY - initialClickY;
        }
        if (resizeLeft) {
          calculatedLeft += event.pageX - initialClickX;
          calculatedWidth -= event.pageX - initialClickX;
        }

        horizonWindowControls.move(calculatedLeft, calculatedTop);
        horizonWindowControls.resize(calculatedWidth, calculatedHeight);

        console.log("Window " + id + " resize");

      }
    };

    document.body.addEventListener("mousemove", mouseMoveListener);

    topBorder.addEventListener("mousedown", (event) => {
      event.preventDefault();
      horizonWindow.classList.add("noAnimation");
      windowResizing = true;
      resizeTop = true;
    });

    topRightCorner.addEventListener("mousedown", (event) => {
      event.preventDefault();
      horizonWindow.classList.add("noAnimation");
      windowResizing = true;
      resizeTop = true;
      resizeRight = true;
    });

    rightBorder.addEventListener("mousedown", (event) => {
      event.preventDefault();
      horizonWindow.classList.add("noAnimation");
      windowResizing = true;
      resizeRight = true;
    });

    bottomRightCorner.addEventListener("mousedown", (event) => {
      event.preventDefault();
      horizonWindow.classList.add("noAnimation");
      windowResizing = true;
      resizeRight = true;
      resizeBottom = true;
    });

    bottomBorder.addEventListener("mousedown", (event) => {
      event.preventDefault();
      horizonWindow.classList.add("noAnimation");
      windowResizing = true;
      resizeBottom = true;
    });

    bottomLeftCorner.addEventListener("mousedown", (event) => {
      event.preventDefault();
      horizonWindow.classList.add("noAnimation");
      windowResizing = true;
      resizeBottom = true;
      resizeLeft = true;
    });

    leftBorder.addEventListener("mousedown", (event) => {
      event.preventDefault();
      horizonWindow.classList.add("noAnimation");
      windowResizing = true;
      resizeLeft = true;
    });

    topLeftCorner.addEventListener("mousedown", (event) => {
      event.preventDefault();
      horizonWindow.classList.add("noAnimation");
      windowResizing = true;
      resizeTop = true;
      resizeLeft = true;
    });

    return horizonWindowControls;

  }

  changeHostBrowserTitle(value) {
    document.title = value;
  }

  subscribeToHostBrowserErrors() {
    window.onerror = function(message, url, lineNumber) {
      console.error("Поймана ошибка, выпавшая в глобальную область!\n" +
        "Сообщение: " + message + "\n(" + url + ":" + lineNumber + ")");
    };
  }

  static async httpGET(url) {
    console.log("Receiving GET " + url);
    return new Promise(((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.send();
      xhr.onreadystatechange = function() {
        if (xhr.readyState !== 4) return;
        console.log("Received " + xhr.status + " " + xhr.statusText + " from GET " + url);
        if (Math.floor(xhr.status / 100) === 2) {
          resolve(xhr);
        } else {
          reject(xhr);
        }

      }
    }));
  }

  static async httpPOST(url, headers = {}, data = {}) {
    console.log("Receiving POST " + url);
    return new Promise(((resolve, reject) => {

      let xhr = new XMLHttpRequest();
      xhr.open('POST', url, true);

      let formData;
      let dataKeys = Object.keys(data);
      if (dataKeys.length > 0) {
        headers["Content-type"] = "application/x-www-form-urlencoded";
        formData = "";
        for (let i = 0; i < dataKeys.length; i++) {
          formData += `${i>0?"&":""}${dataKeys[i]}=${data[dataKeys[i]]}`
        }
      }

      let headersKeys = Object.keys(headers);
      for (let i = 0; i < headersKeys.length; i++) {
        xhr.setRequestHeader(headersKeys[i], headers[headersKeys[i]]);
      }

      xhr.send(formData);

      xhr.onreadystatechange = function() {
        if (xhr.readyState !== 4) return;
        console.log("Received " + xhr.status + " " + xhr.statusText + " from POST " + url);
        if (Math.floor(xhr.status / 100) === 2) {
          resolve(xhr);
        } else {
          reject(xhr);
        }

      }
    }));
  }

  initTranslations() {
    console.log("Initializing Horizon translations...");
    if (typeof this.loadedAssets["translations.json"] !== "object")
      throw new Error("Translations JSON is invalid or not loaded");
    this.translations = this.loadedAssets["translations.json"];
    console.log("Translations loaded");

    this.language = navigator.language.split("-")[0]; // TODO: load language from memory, if it is defined there
    console.info("Switched to language " + this.language);

  }

  getTranslations() {
    return this.translations["LANGUAGES_LIST"];
  }

  changeTranslation(language) {
    this.language = language;
    this.reloadTranslations();
  }

  reloadTranslations() {
    let ids = Object.keys(this.translations);
    for (let i = 0; i < ids.length; i++) {
      let id = ids[i];
      let translation = this.translations[id];
      if (typeof translation["subscriber"] !== "undefined" && typeof translation["subscriber"].innerText !== "undefined") {
        translation["subscriber"].innerText = translation[this.language];
      }
    }
  }

  translate(technicalText, translationID, domSubscriber) {

    if (typeof domSubscriber.innerText === "undefined")
      throw new Error("Invalid DOM subscriber");

    let translation = technicalText;

    if (typeof this.translations[translationID] === "object" && typeof this.translations[translationID][this.language] === "string") {
      translation = this.translations[translationID][this.language];
      this.translations[translationID].subscriber = domSubscriber;
    } else {
      console.warn("Not possible to find translation for " + translationID);
    }

    domSubscriber.innerText = translation;

  }

  getAsset(assetAddress) {
    return this.assetesArray[assetAddress];
  }

  static uiAddSpinner(container) {
    let spinner = document.createElement("div");
    spinner.classList.add("spinner");
    spinner.innerHTML = "<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>";
    container.appendChild(spinner);
  }

}

class API {
  constructor(uri) {
    if (typeof uri === "undefined")
      this.uri = `${window.location.href}api/`
  }
  auth = {
    status: async () => {
      const {
        response
      } = await Horizon.httpGET(`${this.uri}auth/status`);
      const data = JSON.parse(response);
      return !!data.authed;
    },
    auth: async (username, password) => {
      const {
        response
      } = await Horizon.httpPOST(`${this.uri}auth/auth`, undefined, {
        username,
        password
      });
      const data = JSON.parse(response);
      return !!data.authed ? data.user : null;
    },
    deauth: async () => {
      const {
        response
      } = await Horizon.httpGET(`${this.uri}auth/deauth`);
      return !response;
    }
  }
}
// const api = new API();
// await api.auth.status()
// await api.auth.auth("iLeonidze", "123");
